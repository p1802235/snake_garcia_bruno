window.onload = function(){

    // On définit la taille du canvas
    var canvas;
    var canvasWidth = 300;
    var canvasHeight = 300;

    var circle = [0,5];
    var body = [[7,4],[6,4],[5,4],[4,4],[3,4]];
    var blocksize = 20;
    var delay = 100;
    var ctx;
    var snake;
    var apple;

    var positions = "right";
    var end = false;
    var score = 0;
    var level = 0;
    var t = 10;
    var lifes = 0;

    // On récupère les différents éléments du DOM pour les modifier
    var element = document.getElementById('score');
    var element2 = document.getElementById("over");
    var element3 = document.getElementById("level");
    var element4 = document.getElementById('lifes');

    const container = document.createElement('div');
    container.style.display = "flex";
    container.style.justifyContent = "center";
    canvas = this.document.createElement('canvas');
    ctx = canvas.getContext('2d');

    init();

    // Initialisation du canvas
    function init(){
        canvas.style.border = "5px solid";
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        this.document.body.appendChild(container);
        container.appendChild(canvas);
        element2.innerHTML = "Press Enter to play !";
    }

    // Création du canvas
    function newCanvas(){
        ctx.clearRect(0,0,canvasWidth,canvasHeight);
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;

        snake = new setSnake(body,positions);
        apple = new createFood(circle);

        element3.innerHTML = "Level " + level;
        element4.innerHTML = lifes;

        apple.draw();
        snake.draw();

        if(snake.eatItself() && lifes <= 0  || snake.wallCollision()){
            end = true;
            lifes = 0;
            gameOver(false);
        }else{
            if(snake.eatItself()){
                lifes--;
            }

            apple.isAte();
            apple.isOnSnake();
            snake.advence();

            element.innerHTML = score;  

	    // Changement de level
            if(canvasWidth >= 500){
                element2.classList.remove("colorRed");
                element2.classList.add("colorGreen");
                element2.innerHTML = "Now you can cross the walls !";

                snake.passThroughTheWall();
            }

            else    

            snake.wallCollision();
        }
            setTimeout(newCanvas, delay);
    }

    
 
    function drawBlock(ctx,position){
        var img = document.getElementById('image');
        var x = position[0] * blocksize;
        var y = position[1] * blocksize;

        // var xg = headPosition[0] * blocksize;
        // var yg = headPosition[1] * blocksize;
        // ctx.drawImage(img,xg,yg-5,blocksize+10,blocksize+10);

        ctx.fillRect(x,y,blocksize,blocksize);
    }

    function random(min,max){
        min = Math.ceil(min);
        max = Math.floor(max);
        return (Math.floor(Math.random() * (max - min)) + min);
    }

    function gameOver(end){
        element2.innerHTML = "Press Space to restart !";

        if(end){
            location.reload();
        }
    }

    
    // Création du block à manger
    function createFood(body){
        this.body = body;
        ctx.fillStyle = "red";

        this.draw = function (){
            ctx.save();
            var xPosition = this.body[0]*blocksize;
            var yPosition = this.body[1]*blocksize;
            ctx.fillRect(xPosition,yPosition,blocksize,blocksize);
            ctx.restore();
        }

        this.isAte = function (){
            if(snake.body[0][0] == apple.body[0] && snake.body[0][1] == apple.body[1]){
                var randX = random(0,canvasWidth/blocksize-1);
                var randY = random(0,canvasHeight/blocksize-1);
                circle = [randX,randY];
                score++;
                delay--;

                if(score % t == 0){
                    canvasHeight = canvasHeight + 100;
                    canvasWidth = canvasWidth + 100;
                    t = t + 20;
                    level++;
                }

                if(score > 30 && score % 10 == 0){
                    lifes++;
                }
                return true
            }
            return false;
        }

        this.isOnSnake = function(){
            for(var i = 0; i < snake.body.length; i++){
                if(snake.body[i][0] == apple.body[0] && snake.body[i][1]== apple.body[1]){
                    var randX = random(1,canvasWidth/blocksize-1);
                    var randY = random(1,canvasWidth/blocksize-1);
                    circle = [randX,randY];
                }
            }
        }
    }

    

    function setSnake(body,position){
        this.body = body;
        this.position = position;

        this.draw = function(){
            ctx.save();
            ctx.fillStyle = "green";

            for(var i = 0; i < this.body.length; i++){
                drawBlock(ctx,this.body[i]);
            }

            var head = [this.body[0][0]*blocksize,this.body[0][1]*blocksize];
            ctx.restore();
            return head;
        }

        this.advence = function(){
            var newPosition = this.body[0].slice();

            switch (this.position){
                case "left":
                    newPosition[0]--;
                break;
                case 'right':
                    newPosition[0]++;
                break;
                case "up":
                    newPosition[1]--;
                break;
                case "down":
                    newPosition[1]++;
                break;
            }

            this.body.unshift(newPosition);

            if(!apple.isAte()){
                this.body.pop();
            }
        }

        this.setDirection = function(newDirection){
            var possiblePosition = [];

            switch (this.position){
                case "left":
                case 'right':
                    possiblePosition = ["up","down"];
                break;
                case "up":
                case "down":
                    possiblePosition = ["right","left"];
                break;
            }

            for(var i = 0; i < possiblePosition.length; i++){
                if(newDirection === possiblePosition[i]){
                    positions = newDirection;
                }
            }
        }

        this.passThroughTheWall = function (){
            var minX = 0;
            var minY = 0;
            var maxX = canvasWidth/blocksize;
            var maxY = canvasHeight/blocksize;
            var turnBack = this.body[0].slice();
            var touchMaxX = this.body[0][0] > maxX-1;
            var touchMinX = this.body[0][0] < minX;
            var touchMaxY = this.body[0][1] > maxY-1;
            var touchMinY = this.body[0][1] < minY;

            switch(true){
                case touchMaxX:
                    turnBack[0] = minX;
                break;
                case touchMinX:
                    turnBack[0] = maxX-1;
                break;
                case touchMaxY:
                    turnBack[1] = minY;
                break;
                case touchMinY:
                    turnBack[1] = maxY-1;
                break;
            }

            if(touchMaxX || touchMaxY || touchMinX || touchMinY){
                this.body.unshift(turnBack);
                this.body.pop();
            }
        }

        this.wallCollision = function (){
            var minX = 0;
            var minY = 0;
            var maxX = canvasWidth/blocksize;
            var maxY = canvasHeight/blocksize;

            var headArray = this.body[0].slice();
            var headX = headArray[0];
            var headY = headArray[1];

            if(headX == minX-1 || headY == minY-1 || headX == maxX || headY == maxY){
                return true;
            }
            return false;
        }

        this.eatItself = function(){
            for(var i = 1; i < this.body.length-1; i++){
                if(this.body[0][0] == this.body[i][0] && this.body[0][1] == this.body[i][1]){
                    return true;
                }
            }
            return false;
        }
    }

    this.document.onkeydown = function handleKeyDown(e){
        var newDirection;
        var key = e.keyCode;
	
	// On récupère les codes des touches du clavier pour se déplacer
        switch(key){
            case 37:
                newDirection = "left";
            break;
            case 38:
                newDirection = "up";
            break;
            case 39:
                newDirection = "right";
            break;
            case 40:
                newDirection = "down";
            break;
            case 13:
                element2.classList.add("colorRed");
                element2.innerHTML = "Don't touch the walls !";
                newCanvas();
            break;
            case 32:
                if(end == true){
                    gameOver(end);
                }
            break;

        }
        snake.setDirection(newDirection);
    }
}
